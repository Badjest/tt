package com.android.tt;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.tt.fragments.fragmentMap;

public class MapActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

        changeFragment(fragmentMap.class, getSupportFragmentManager(), R.anim.fade_in,R.anim.fade_out);
    }

    public void changeFragment(Class item, FragmentManager fragmentManager, Integer inAnim , Integer outAnim)  {

        Fragment fragment;
        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                    .replace(R.id.fl_content, fragment)
                    .setCustomAnimations(inAnim,outAnim)
                    .addToBackStack("detail")
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString() , Toast.LENGTH_SHORT).show();
        }

    }
}
