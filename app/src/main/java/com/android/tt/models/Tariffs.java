package com.android.tt.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 14.05.2017.
 */

public class Tariffs {
    private Integer id;
    private String name; //название тарифа
    private Integer beginPrice; // посадка в авто (минимальная цена проезда)
    private Integer freeWait; // время бесплатного ожидания в минутах
    private Integer priceWait; // стоимость минуты ожидания за пределами беслатного ожидания
    private Integer priceCityKm; // стоимость километра по городу
    private Integer priceCityMin; // стоимость минуты по городу
    private Integer timeStartTariff; // время начала тарифа от 0 до 24
    private Integer timeEndTariff; // время окончание тарифа от 0 до 24
    private String description; // описание тарифа

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBeginPrice() {
        return beginPrice;
    }

    public void setBeginPrice(Integer beginPrice) {
        this.beginPrice = beginPrice;
    }

    public Integer getFreeWait() {
        return freeWait;
    }

    public void setFreeWait(Integer freeWait) {
        this.freeWait = freeWait;
    }

    public Integer getPriceWait() {
        return priceWait;
    }

    public void setPriceWait(Integer priceWait) {
        this.priceWait = priceWait;
    }

    public Integer getPriceCityKm() {
        return priceCityKm;
    }

    public void setPriceCityKm(Integer priceCityKm) {
        this.priceCityKm = priceCityKm;
    }

    public Integer getPriceCityMin() {
        return priceCityMin;
    }

    public void setPriceCityMin(Integer priceCityMin) {
        this.priceCityMin = priceCityMin;
    }

    public Integer getTimeStartTariff() {
        return timeStartTariff;
    }

    public void setTimeStartTariff(Integer timeStartTariff) {
        this.timeStartTariff = timeStartTariff;
    }

    public Integer getTimeEndTariff() {
        return timeEndTariff;
    }

    public void setTimeEndTariff(Integer timeEndTariff) {
        this.timeEndTariff = timeEndTariff;
    }


    public ArrayList<Tariffs> getArrayListOfTariffs(JSONArray array) {
        ArrayList<Tariffs> _list = new ArrayList<Tariffs>();

        JSONObject jsonData = null;
        try {

            for(int i = 0; i < array.length() ; i++) {

                jsonData = array.getJSONObject(i);
                Tariffs a = new Tariffs();
                a.setId(jsonData.getInt("ID"));
                a.setName(jsonData.getString("name"));
                a.setBeginPrice(jsonData.getInt("begin_price"));
                a.setFreeWait(jsonData.getInt("free_wait"));
                a.setPriceWait(jsonData.getInt("price_wait"));
                a.setPriceCityKm(jsonData.getInt("price_city_km"));
                a.setPriceCityMin(jsonData.getInt("price_city_min"));
                a.setTimeEndTariff(jsonData.getInt("time_end_tariff"));
                a.setTimeStartTariff(jsonData.getInt("time_start_tariff"));
                a.setDescription(jsonData.getString("description"));

                _list.add(a);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return _list;
    }

    public float Raschet(float rastMetr) {
        float price = 0;
        float km = rastMetr / 1000;
        price += beginPrice + priceCityKm * km;
        return price;
    }
}
