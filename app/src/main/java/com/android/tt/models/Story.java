package com.android.tt.models;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 14.05.2017.
 */

public class Story {
    private String From;
    private String To;
    private LatLng coordFrom;
    private LatLng coordTo;
    private Integer idTariff;

    public String getTo() {
        return To;
    }

    public void setTo(String to) {
        To = to;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public LatLng getCoordFrom() {
        return coordFrom;
    }

    public void setCoordFrom(LatLng coordFrom) {
        this.coordFrom = coordFrom;
    }

    public LatLng getCoordTo() {
        return coordTo;
    }

    public void setCoordTo(LatLng coordTo) {
        this.coordTo = coordTo;
    }

    public Integer getIdTariff() {
        return idTariff;
    }

    public void setIdTariff(Integer idTariff) {
        this.idTariff = idTariff;
    }

    public ArrayList<Story> getArrayListOfStory(JSONArray array) {
        ArrayList<Story> _list = new ArrayList<Story>();

        JSONObject jsonData = null;
        try {

            for(int i = 0; i < array.length() ; i++) {

                jsonData = array.getJSONObject(i);
                Story a = new Story();
                a.setFrom(jsonData.getString("point_a"));
                a.setTo(jsonData.getString("point_b"));
                a.setIdTariff(jsonData.getInt("id_tariff"));
                LatLng from = new LatLng(jsonData.getDouble("x1"),jsonData.getDouble("y1"));
                a.setCoordFrom(from);
                LatLng to = new LatLng(jsonData.getDouble("x2"),jsonData.getDouble("y2"));
                a.setCoordTo(to);
                _list.add(a);
            }

        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return _list;
    }

}
