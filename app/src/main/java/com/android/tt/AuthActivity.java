package com.android.tt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.widget.Toast;

import com.android.tt.fragments.fragmentAuthStepOne;
import com.digits.sdk.android.Digits;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

public class AuthActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String phone = preferences.getString( "phoneNumber" , null);

        if (phone == null) {
            changeFragment(fragmentAuthStepOne.class, getSupportFragmentManager(), R.anim.fade_in,R.anim.fade_out);
        } else {
            Intent intent = new Intent(this, MapActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void changeFragment(Class item, FragmentManager fragmentManager, Integer inAnim ,Integer outAnim)  {

        Fragment fragment;
        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                    .replace(R.id.fl_content, fragment)
                    .setCustomAnimations(inAnim,outAnim)
                    .addToBackStack("detail")
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.toString() , Toast.LENGTH_SHORT).show();
        }

    }
}
