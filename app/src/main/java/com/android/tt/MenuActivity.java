package com.android.tt;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;

import com.android.tt.fragments.fragmentMenu;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        changeFragmentWithoutSave(fragmentMenu.class, getSupportFragmentManager());
    }

    public void changeFragment(Class item, FragmentManager fragmentManager)  {
        Fragment fragment;

        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_right,  R.anim.slide_in_left, R.anim.slide_out_right)
                        .replace(R.id.fl_content, fragment)
                        .addToBackStack("detail")
                        .commit();

            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void changeFragmentWithoutSave(Class item, FragmentManager fragmentManager)  {
        Fragment fragment;

        try {
            fragment = (Fragment) item.newInstance();
            fragmentManager.beginTransaction()
                    .replace(R.id.fl_content, fragment)
                    .commit();

            getSupportFragmentManager().addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        public void onBackStackChanged() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
