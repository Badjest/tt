package com.android.tt.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.MapActivity;
import com.android.tt.MenuActivity;
import com.android.tt.R;
import com.android.tt.fragments.fragmentOrderDetail;
import com.android.tt.models.Story;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 14.05.2017.
 */

public class rvStories extends RecyclerView.Adapter<rvStories.ViewHolder> {

    private ArrayList<Story> mDataset;
    private Context context;
    private SharedPreferences sharedPref = null;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView from;
        public TextView to;
        public ImageView repeat;

        public ViewHolder(View v) {
            super(v);
            from = (TextView) v.findViewById(R.id.fromText);
            to = (TextView) v.findViewById(R.id.toText);
            repeat = (ImageView) v.findViewById(R.id.repeatButton);
        }
    }

    // Конструктор
    public rvStories(ArrayList<Story> dataset, Context context) {
        mDataset = dataset;
        this.context = context;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public rvStories.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_stories, parent, false);

        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.from.setText(mDataset.get(position).getFrom());
        holder.to.setText(mDataset.get(position).getTo());

        holder.repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = sharedPref.edit();
                Gson gson = new Gson();
                String json = gson.toJson(mDataset.get(position).getCoordTo());
                editor.putString("orderLatLngTo", json);
                json = gson.toJson(mDataset.get(position).getCoordFrom());
                editor.putString("orderLatLngFrom", json);
                editor.putString("orderPlaceNameFrom",mDataset.get(position).getFrom());
                editor.putString("orderPlaceNameTo",mDataset.get(position).getTo());
                editor.commit();
                ((MenuActivity) context).changeFragment(fragmentOrderDetail.class, ((MenuActivity)context).getSupportFragmentManager());
            }
        });

    }



    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
