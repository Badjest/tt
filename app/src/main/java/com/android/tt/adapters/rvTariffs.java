package com.android.tt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.R;
import com.android.tt.models.Story;
import com.android.tt.models.Tariffs;

import java.util.ArrayList;

/**
 * Created by JoshRonaldMine on 14.05.2017.
 */

public class rvTariffs extends RecyclerView.Adapter<rvTariffs.ViewHolder> {

    private ArrayList<Tariffs> mDataset;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tariffName;
        public TextView tariffTime;
        public TextView tariffBeginPrice;
        public TextView tariffFreeWait;
        public TextView tariffPriceWait;
        public TextView tariffPriceCiryKm;
        public TextView tariffPriceCiryMin;
        public TextView tariffDescr;


        public ViewHolder(View v) {
            super(v);
            tariffName = (TextView) v.findViewById(R.id.tariffName);
            tariffTime = (TextView) v.findViewById(R.id.tariffTime);
            tariffBeginPrice = (TextView) v.findViewById(R.id.tariffBeginPrice);
            tariffFreeWait = (TextView) v.findViewById(R.id.tariffFreeWait);
            tariffPriceWait = (TextView) v.findViewById(R.id.tariffPriceWait);
            tariffPriceCiryKm = (TextView) v.findViewById(R.id.tariffPriceCiryKm);
            tariffPriceCiryMin = (TextView) v.findViewById(R.id.tariffPriceCiryMin);
            tariffDescr = (TextView) v.findViewById(R.id.tariffDescr);
        }
    }

    // Конструктор
    public rvTariffs(ArrayList<Tariffs> dataset, Context context) {
        mDataset = dataset;
        this.context = context;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public rvTariffs.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_item_tariffs, parent, false);

        context = parent.getContext();

        // тут можно программно менять атрибуты лэйаута (size, margins, paddings и др.)

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.tariffName.setText(mDataset.get(position).getName());
        holder.tariffTime.setText("время действия: с "
                + mDataset.get(position).getTimeStartTariff().toString()
                +":00 до "
                + mDataset.get(position).getTimeEndTariff().toString()+":00");
        holder.tariffBeginPrice.setText(mDataset.get(position).getBeginPrice() + " руб.");
        holder.tariffFreeWait.setText(mDataset.get(position).getFreeWait() + " мин.");
        holder.tariffPriceWait.setText(mDataset.get(position).getPriceWait() + " руб/мин.");
        holder.tariffPriceCiryKm.setText(mDataset.get(position).getPriceCityKm() + " руб/км.");
        holder.tariffPriceCiryMin.setText(mDataset.get(position).getPriceCityMin() + " руб/мин.");
        holder.tariffDescr.setText(mDataset.get(position).getDescription());
    }



    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
