package com.android.tt.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.android.tt.R;
import com.android.tt.models.Tariffs;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by JoshRonaldMine on 19.05.2017.
 */

public class rvTariffsSelect extends RecyclerView.Adapter<rvTariffsSelect.ViewHolder>{
    private ArrayList<Tariffs> mDataset;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.tariffName);

        }
    }

    // Конструктор
    public rvTariffsSelect(ArrayList<Tariffs> dataset, Context context) {
        mDataset = dataset;
        this.context = context;
    }

    // Создает новые views (вызывается layout manager-ом)
    @Override
    public rvTariffsSelect.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_tariffs_sel, parent, false);

        context = parent.getContext();

        rvTariffsSelect.ViewHolder vh = new rvTariffsSelect.ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(rvTariffsSelect.ViewHolder holder, final int position) {
        holder.name.setText(mDataset.get(position).getName());
    }

    public Tariffs getItemTariff(final int pos) {
        return mDataset.get(pos);
    }

    // Возвращает размер данных (вызывается layout manager-ом)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
