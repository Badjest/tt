package com.android.tt.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.MapActivity;
import com.android.tt.R;
import com.android.tt.classes.Config;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.AuthConfig;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentAuthStepOne extends Fragment {

    EditText phone;
    ImageButton nextStep;
    ProgressBar preload;
    TextInputLayout til;
    TextView hello;
    TextView warn;


    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "xbyXwR5knmWJCK1gKTujttH24";
    private static final String TWITTER_SECRET = "qTRk6CZhSoFoxGQCm59bnbP3l8neKx8cY0dkFc4FS1pjP7M5Tq";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(getActivity(), new TwitterCore(authConfig), new Digits.Builder().withTheme(R.style.CustomDigitsTheme).build());
        final View v = inflater.inflate(R.layout.fragment_auth_step1, container, false);



        Typeface RalewayLight  = Typeface.createFromAsset(getContext().getAssets(), "fonts/RalewayLight.ttf");

        phone = (EditText) v.findViewById(R.id.number);
        phone.setTypeface(RalewayLight);
        preload = (ProgressBar) v.findViewById(R.id.progressBar);
        til = (TextInputLayout) v.findViewById(R.id.til);
        til.setTypeface(RalewayLight);
        hello = (TextView) v.findViewById(R.id.hello);
        warn = (TextView) v.findViewById(R.id.warn);

        animateViewFadeIn(hello, 200);
        animateViewFadeIn(til, 500);
        animateViewFadeIn(warn, 800);


        // Кнопка Следующий шаг
        nextStep =  (ImageButton) v.findViewById(R.id.nextBut);
        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String check = check_name(phone.getText().toString());
                if (check.length() > 0) {
                    til.setErrorEnabled(true);
                    til.setError(check);
                    return;
                } else {
                    til.setErrorEnabled(false);
                    til.setError(null);
                }

                preload_on();

                AuthConfig.Builder authConfigBuilder = new AuthConfig.Builder()
                        .withAuthCallBack(callback)
                        .withPhoneNumber("+7");


                Digits.authenticate(authConfigBuilder.build());
            }
        });

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        return v;
    }

    /*
    * Callback для аутентификации номера
    * */
    final AuthCallback callback = new AuthCallback() {
        @Override
        public void success(DigitsSession session, final String phoneNumber) {

            TwitterAuthConfig authConfig = TwitterCore.getInstance().getAuthConfig();
            TwitterAuthToken authToken = session.getAuthToken();
            DigitsOAuthSigning oauthSigning = new DigitsOAuthSigning(authConfig, authToken);
            Map<String, String> authHeaders = oauthSigning.getOAuthEchoHeadersForVerifyCredentials();


            OkHttpClient client = new OkHttpClient();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(new Config().getFullApiUrl() + "add_user/").newBuilder();
            JSONObject params = new JSONObject();
            preload_on();

            try {
                params.put("name", phone.getText().toString());
                params.put("oauth_access_token", authToken.token.toString());
                params.put("url", authHeaders.get("X-Auth-Service-Provider"));
                params.put("oauth_access_token_secret", authToken.secret.toString());
            } catch (JSONException js) {
                js.printStackTrace();
            }
            RequestBody formBody = new FormBody.Builder()
                    .add("params", params.toString())
                    .build();
            Request request = new Request.Builder()
                    .url(urlBuilder.build().toString())
                    .post(formBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, final IOException e) {
                    e.printStackTrace();
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), e.getMessage() , Toast.LENGTH_SHORT).show();
                        }
                    });

                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    } else {
                        String body = response.body().string();
                        try {
                            JSONObject answer = new JSONObject(body);
                            final String messageOrToken = (answer.has("message"))? answer.getString("message") : null ;

                            if (answer.has("success")) {

                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        save_and_start_map_activity(phoneNumber, messageOrToken);
                                    }
                                });
                            }
                            if (answer.has("error")) {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getActivity(), messageOrToken, Toast.LENGTH_LONG).show();
                                        preload_off();
                                    }
                                });

                            }
                        } catch (JSONException js) {
                            js.printStackTrace();
                        }
                    }

                }
            });

        }
        @Override
        public void failure(DigitsException exception) {
            preload_off();
            Log.d("Digits", "Sign in with Digits failure", exception);
        }
    };

    /*
    * Сохраняет телефон и имя пользователя в настройках
    * Старт новой активити
    * */
    private void save_and_start_map_activity(final String phoneNumber, final String token) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("phoneNumber", phoneNumber.substring(0,4) + "*****" + phoneNumber.substring(phoneNumber.length() - 4,phoneNumber.length()));
        editor.putString("token", token);
        editor.putString("userName", phone.getText().toString());
        editor.putBoolean("notCall", false);
        editor.putBoolean("notSms", false);
        editor.putInt("tip",0);
        editor.commit();

        Intent intent = new Intent(getActivity(), MapActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    /*
    Метод проверки корректности ввода имени
    @param: текст
    Возвращает пустой текст, если всё ок, ошибку, если не ок
    */
    private String check_name(String name) {
        if (name.length() == 0) return "Это поля обязательно!";
        if (name.length() == 1) return "Имя из одной буквы? Правда?";
        return "";
    }

    /*
    * Анимация
    * */
    private void animateViewFadeIn(View view, Integer delay) {
        AlphaAnimation animation = new AlphaAnimation(0.0f,1.0f);
        animation.setDuration(500);
        animation.setStartOffset(delay);
        view.startAnimation(animation);
    }

    /*
    * Включение прелоадера
    * */
    private void preload_on() {
        nextStep.setVisibility(View.GONE);
        preload.setVisibility(View.VISIBLE);
    }

    /*
    * Выключение прелоадера
    * */
    private void preload_off() {
        nextStep.setVisibility(View.VISIBLE);
        preload.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        //preload_off();
    }
}
