package com.android.tt.fragments.menuFragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.R;
import com.android.tt.classes.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenuTips extends Fragment {

    private RadioGroup radioButtonGroup;
    private  ProgressDialog loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu_tips, container, false);


        radioButtonGroup = (RadioGroup) v.findViewById(R.id.radioGroup);



        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        Integer tipSh = sharedPref.getInt("tip", 0);
        switch (tipSh) {
            case 0:
                radioButtonGroup.check(R.id.radio_0);
                break;
            case 5:
                radioButtonGroup.check(R.id.radio_5);
                break;
            case 15:
                radioButtonGroup.check(R.id.radio_15);
                break;
            case 20:
                radioButtonGroup.check(R.id.radio_20);
                break;
        }


        v.findViewById(R.id.saveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
                View radioButton = radioButtonGroup.findViewById(radioButtonID);
                final int idx = radioButtonGroup.indexOfChild(radioButton);

                OkHttpClient client = new OkHttpClient();
                HttpUrl.Builder urlBuilder = HttpUrl.parse(new Config().getFullApiUrl() + "save_user_stats/").newBuilder();
                JSONObject params = new JSONObject();

                try {
                    switch (idx) {
                        case 0:
                            params.put("tips", 0 );
                            break;
                        case 1:
                            params.put("tips", 5 );
                            break;
                        case 2:
                            params.put("tips", 15 );
                            break;
                        case 3:
                            params.put("tips", 20);
                            break;
                    }
                    params.put("token", sharedPref.getString("token", ""));
                } catch (JSONException js) {
                    Toast.makeText(getActivity(), js.getMessage(), Toast.LENGTH_SHORT).show();
                    js.printStackTrace();
                }

                RequestBody formBody = new FormBody.Builder()
                        .add("params", params.toString())
                        .build();
                Request request = new Request.Builder()
                        .url(urlBuilder.build().toString())
                        .post(formBody)
                        .build();

                preload_on();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, final IOException e) {
                        e.printStackTrace();
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getActivity(), e.getMessage() , Toast.LENGTH_SHORT).show();
                                preload_off();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            preload_off();
                            throw new IOException("Unexpected code " + response);
                        } else {
                            String body = response.body().string();
                            try {
                                JSONObject answer = new JSONObject(body);
                                final String message = (answer.has("message"))? answer.getString("message") : null ;

                                if (answer.has("success")) {
                                    preload_off();
                                    save_and_back(idx,sharedPref);
                                }
                                if (answer.has("error")) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            preload_off();
                                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                        }
                                    });

                                }
                            } catch (JSONException js) {
                                js.printStackTrace();
                                preload_off();
                                show_message(js.getMessage());
                            }
                        }
                    }
                });
            }
        });
        return v;
    }

    private void save_and_back(Integer idx, SharedPreferences sharedPref ) {
        SharedPreferences.Editor editor = sharedPref.edit();
        switch (idx) {
            case 0:
                editor.putInt("tip", 0 );
                break;
            case 1:
                editor.putInt("tip", 5 );
                break;
            case 2:
                editor.putInt("tip", 15 );
                break;
            case 3:
                editor.putInt("tip", 20 );
                break;
        }
        editor.commit();
        getFragmentManager().popBackStack();
    }

    private void preload_on() {
        loading = ProgressDialog.show(getActivity(),"Сохраняем","Пожалуйста, подождите.",false,false);
    }

    private void preload_off() {
        if (loading.isShowing()) {
            loading.dismiss();
        }
    }

    private void show_message(final String message){
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

            }
        });
    }

}
