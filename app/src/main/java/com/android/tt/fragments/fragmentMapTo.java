package com.android.tt.fragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.MapActivity;
import com.android.tt.MenuActivity;
import com.android.tt.R;
import com.android.tt.adapters.rvTariffsSelect;
import com.android.tt.classes.Config;
import com.android.tt.classes.returnJsonArray;
import com.android.tt.models.Tariffs;
import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import at.markushi.ui.CircleButton;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMapTo extends Fragment {


    MapView mMapView;
    private GoogleMap googleMap;
    Geocoder geocoder;
    List<Address> addresses;
    private TextView address;
    private TextView openSearch;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private SharedPreferences sharedPref = null;
    private MaterialRippleLayout nextStep;
    private LatLng latLng = null;
    private String namePlace = null;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_map_to, container, false);

        mMapView = (MapView) v.findViewById(R.id.mapView);
        address = (TextView) v.findViewById(R.id.address);
        openSearch = (TextView) v.findViewById(R.id.my_adress);
        nextStep = (MaterialRippleLayout) v.findViewById(R.id.nextStep);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mMapView.onCreate(savedInstanceState);


        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {

                googleMap = mMap;
                googleMap.clear();
                Marker marker = googleMap.addMarker(new MarkerOptions().position(googleMap.getCameraPosition().target));
                marker.setVisible(false);
                address.setText(geocode(marker));

                /* Отключение кнопки компаса с моего местоположения */
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(false);
                standart_start_map();

                googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
                        hide_elements_animations(500);
                    }
                });



                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        show_elements_animations(500);
                        googleMap.clear();
                        latLng = googleMap.getCameraPosition().target;
                        Marker marker = googleMap.addMarker(new MarkerOptions().position(googleMap.getCameraPosition().target));
                        marker.setVisible(false);
                        address.setText(geocode(marker));
                    }
                });
                go_to_me();
            }
        });

        /* start fade in */
        show_elements_animations(1500);


        /**/
        openSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    show_toast_thread(e.getMessage());
                } catch (GooglePlayServicesNotAvailableException e) {
                    show_toast_thread(e.getMessage());
                }
            }
        });

        /*кнопка далее*/
        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPref.edit();
                Gson gson = new Gson();
                String json = gson.toJson(latLng);
                if (namePlace!=null) {
                    editor.putString("orderPlaceNameTo",namePlace);
                }
                editor.putString("orderLatLngTo", json);
                editor.commit();
                ((MapActivity) getActivity()).changeFragment(fragmentOrderDetail.class, getActivity().getSupportFragmentManager(), R.anim.fade_in,R.anim.fade_out);
            }
        });

        return v;
    }




    /*Результат после поиска адреса*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                googleMap.clear();
                latLng = place.getLatLng();
                Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng));
                marker.setVisible(false);
                address.setText(geocode(marker));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                googleMap.moveCamera(cameraUpdate);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                show_toast_thread(status.getStatusMessage());
            }
        }
    }


    /* портирует камеру к gпредыдущей точке */
    private void go_to_me() {
        Gson gson = new Gson();
        String json = sharedPref.getString("orderLatLngFrom", null);
        if (json != null) {
            LatLng from = gson.fromJson(json, LatLng.class);
            googleMap.clear();
            Marker marker = googleMap.addMarker(new MarkerOptions().position(from));
            marker.setVisible(false);
            address.setText(geocode(marker));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(from)
                    .zoom(13)
                    .build();
            MapFragment.newInstance(new GoogleMapOptions()
                    .camera(cameraPosition));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(from, 14);
            googleMap.animateCamera(cameraUpdate);
        } else {
            show_toast_thread("Нет координат от");
            standart_start_map();
        }

    }


    /* show active elements */
    private void show_elements_animations(Integer duration) {
        fade_in(address, duration);
        fade_in(openSearch,duration);
        SlideToAbove(nextStep, duration);
    }

    /* показ тоста в треде*/
    private void show_toast_thread(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /* hide active elements*/
    private void hide_elements_animations(Integer duration) {
        fade_out(address, duration);
        fade_out(openSearch,duration);
        SlideToDown(nextStep, duration);
    }

    /* fade in animation */
    private void fade_in(final View iv, Integer duration) {
        iv.animate()
                .alpha(1.0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });
    }

    public void SlideToDown(View view , Integer duration) {
        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 5.2f);


        slide.setDuration(duration);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);
    }


    public void SlideToAbove(View view , Integer duration) {
        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                5.2f, Animation.RELATIVE_TO_SELF, 0.0f);


        slide.setDuration(duration);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);
    }

    /* fade out animation */
    private void fade_out(final View iv, Integer duration) {
        iv.animate()
                .alpha(0.0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });
    }

    /* определение адреса по координатам */
    private String geocode(Marker this_mark) {
        String address = "";
        try {
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            addresses = geocoder.getFromLocation(this_mark.getPosition().latitude, this_mark.getPosition().longitude, 1);
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
            } else {
                address = "Не определено";
            }

            namePlace = address;

        } catch (IOException exception) {
            Log.e("mapApp", exception.toString());
        }
        return address;
    }


    /* Старт карты без GPS */
    private void standart_start_map() {
        LatLngBounds BARNAUL = new LatLngBounds(
                new LatLng(53.3556, 83.516074), new LatLng(53.367796, 83.834152));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BARNAUL.getCenter(), 10));
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    standart_start_map();
                } else {
                    Toast.makeText(getActivity(), "Разрешение не было получено!", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
