package com.android.tt.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.MapActivity;
import com.android.tt.MenuActivity;
import com.android.tt.R;
import com.android.tt.classes.Config;
import com.android.tt.fragments.menuFragments.fragmentMenuAddCard;
import com.android.tt.fragments.menuFragments.fragmentMenuPhone;
import com.android.tt.fragments.menuFragments.fragmentMenuShowCard;
import com.android.tt.fragments.menuFragments.fragmentMenuStories;
import com.android.tt.fragments.menuFragments.fragmentMenuTariffs;
import com.android.tt.fragments.menuFragments.fragmentMenuTips;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.AuthConfig;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsOAuthSigning;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenu extends Fragment {

    private LinearLayout phone;
    private TextView phoneTV;
    private TextView tipTV;
    private Switch noCall;
    private Switch noSms;
    private LinearLayout tipLL;
    private LinearLayout storyLL;
    private LinearLayout tariffsLL;
    private LinearLayout bankLL;
    private TextView tvBank;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu, container, false);

        /* Телефон */
        phone = (LinearLayout) v.findViewById(R.id.phone);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity) getActivity()).changeFragment(fragmentMenuPhone.class, getActivity().getSupportFragmentManager());
            }
        });
        phoneTV = (TextView) v.findViewById(R.id.phoneNumber);
        tipTV = (TextView) v.findViewById(R.id.tip);
        noCall  = (Switch) v.findViewById(R.id.notCall);
        noSms  = (Switch) v.findViewById(R.id.notSms);
        tipLL  = (LinearLayout) v.findViewById(R.id.tipClick);
        tvBank = (TextView) v.findViewById(R.id.tvBank);
        storyLL  = (LinearLayout) v.findViewById(R.id.storyClick);
        tariffsLL  = (LinearLayout) v.findViewById(R.id.tariffsClick);
        bankLL  = (LinearLayout) v.findViewById(R.id.bankClick);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String phoneSh = sharedPref.getString("phoneNumber", "");
        Integer tipSh = sharedPref.getInt("tip", 0);
        Boolean noCallSh = sharedPref.getBoolean("notCall", false);
        Boolean noSmsSh = sharedPref.getBoolean("notSms", false);



        if (sharedPref.getString("bankBrand", null)  == null) {
            tvBank.setText("Добавить банковскую карту");
        }


        phoneTV.setText(phoneSh);
        noCall.setChecked(noCallSh);
        noSms.setChecked(noSmsSh);
        tipTV.setText(tipSh.toString() + "%");

        /* Switch для звонков */
        noCall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("notCall", isChecked);
                editor.commit();
            }
        });

        /* Switch для звонков */
        noSms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("notSms", isChecked);
                editor.commit();
            }
        });

        /* Клик чаевые */
        tipLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity) getActivity()).changeFragment(fragmentMenuTips.class, getActivity().getSupportFragmentManager());
            }
        });

        tariffsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity) getActivity()).changeFragment(fragmentMenuTariffs.class, getActivity().getSupportFragmentManager());
            }
        });

        /* Клик банковская карта */
        bankLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bankToken = sharedPref.getString("savedCard", null);
                if (bankToken  == null) {
                    ((MenuActivity) getActivity()).changeFragment(fragmentMenuAddCard.class, getActivity().getSupportFragmentManager());
                } else {
                    ((MenuActivity) getActivity()).changeFragment(fragmentMenuShowCard.class, getActivity().getSupportFragmentManager());
                }

            }
        });

        /* Клик история поездок */
        storyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MenuActivity) getActivity()).changeFragment(fragmentMenuStories.class, getActivity().getSupportFragmentManager());
            }
        });


        /* Нажатие НАЗАД */
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK ) {
                    getActivity().overridePendingTransition( R.anim.fade_in , R.anim.fade_out );
                    getActivity().finish();
                    return true;
                } else {
                    return false;
                }
            }
        });
        return v;
    }





}
