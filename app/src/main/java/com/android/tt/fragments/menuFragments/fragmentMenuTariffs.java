package com.android.tt.fragments.menuFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.R;
import com.android.tt.adapters.rvStories;
import com.android.tt.adapters.rvTariffs;
import com.android.tt.classes.Config;
import com.android.tt.classes.returnJsonArray;
import com.android.tt.models.Story;
import com.android.tt.models.Tariffs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenuTariffs extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Tariffs> listTariffs = new ArrayList<>();
    private ProgressBar progress;
    private TextView errors;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu_tariffs, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_tariffs);
        progress = (ProgressBar) v.findViewById(R.id.preload);
        errors = (TextView) v.findViewById(R.id.error);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(new Config().getFullApiUrl() + "tariffs/")
                    .build();

        preload_on();

            client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        preload_off();
                        e.printStackTrace();
                        final IOException fe = e;
                        error_on(fe.getMessage());
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String body = response.body().string();
                        preload_off();
                        try {
                            JSONObject answer = new JSONObject(body);

                            if (answer.has("success") && answer.has("result")) {

                                JSONArray listFromWeb =  new returnJsonArray().getJsonArray(answer.getString("result"));
                                listTariffs = new Tariffs().getArrayListOfTariffs(listFromWeb);

                                try {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            mAdapter = new rvTariffs(listTariffs, getActivity());
                                            mRecyclerView.setAdapter(mAdapter);
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    error_on(e.getMessage());
                                }

                            }
                            if (answer.has("error")) {
                                preload_off();
                                if (answer.has("message")) {
                                    error_on(answer.getString("message"));
                                } else {
                                    error_on("Не удалось загрузить");
                                }

                            }
                        } catch (JSONException js) {
                            js.printStackTrace();
                        }
                    }
                });





        return v;
    }

    private void preload_on() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                progress.setVisibility(View.VISIBLE);
            }
        });

    }

    private void preload_off() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void error_on(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                errors.setVisibility(View.VISIBLE);
                errors.setText(message);
            }
        });
    }

    private void error_off() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                errors.setVisibility(View.GONE);
            }
        });
    }

}
