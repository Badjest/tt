package com.android.tt.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.MapActivity;
import com.android.tt.MenuActivity;
import com.android.tt.R;
import com.android.tt.classes.Config;
import com.android.tt.fragments.menuFragments.fragmentMenuTips;
import com.android.tt.models.Tariffs;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentOrderDetail extends Fragment {

    private TextView fromDetail;
    private TextView toDetail;

    private TextView tipsL;
    private TextView tariffL;
    private SharedPreferences sharedPref = null;
    private LinearLayout anim2, anim3,anim4,anim5,anim6;
    private Button zakaz;
    private double x1,y1,x2,y2,price;
    private String point_a, point_b;
    private Card cardForPay = null;
    private Tariffs tariffUsed = null;
    private  ProgressDialog loading;
    private LinearLayout changeTips;
    private RadioGroup groupCash;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_order_detail, container, false);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        anim2 = (LinearLayout) v.findViewById(R.id.anim2);
        anim3 = (LinearLayout) v.findViewById(R.id.anim3);
        anim4 = (LinearLayout) v.findViewById(R.id.anim4);
        anim5 = (LinearLayout) v.findViewById(R.id.anim5);
        anim6 = (LinearLayout) v.findViewById(R.id.anim6);
        fromDetail = (TextView) v.findViewById(R.id.fromDetail);
        toDetail = (TextView) v.findViewById(R.id.toDetail);

        tipsL = (TextView) v.findViewById(R.id.tips);
        tariffL = (TextView) v.findViewById(R.id.tariff);
        zakaz = (Button) v.findViewById(R.id.zakaz);
        changeTips = (LinearLayout) v.findViewById(R.id.changeTips);
        groupCash = (RadioGroup) v.findViewById(R.id.radioGroup);

        load_detail();
        start_load_animation();



        /*
        * Смена чаевых
        * */
        changeTips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String activityName = getActivity().getClass().getSimpleName();
                if (activityName.equals("MapActivity")) {
                    ((MapActivity) getActivity()).changeFragment(fragmentMenuTips.class, getActivity().getSupportFragmentManager(), R.anim.fade_in,R.anim.fade_out);
                } else if (activityName.equals("MenuActivity")) {
                    ((MenuActivity) getActivity()).changeFragment(fragmentMenuTips.class, getActivity().getSupportFragmentManager());
                }
            }
        });

        /*
        * делаем заказ
        * */
        v.findViewById(R.id.doOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                preload_on();
                int radioButtonID = groupCash.getCheckedRadioButtonId();
                View radioButton = groupCash.findViewById(radioButtonID);
                final int idx = groupCash.indexOfChild(radioButton);

                switch (idx) {
                    case 0:
                        send_to_server("nal");
                        break;
                    case 1:
                        Stripe stripe = new Stripe(getActivity(), "pk_test_u1d3M4fFTcPUUX609AbZVkTN");
                        stripe.createToken(
                                cardForPay,
                                new TokenCallback() {
                                    public void onSuccess(Token token) {
                                        send_to_server(token.getId());
                                    }
                                    public void onError(Exception error) {
                                        preload_off();
                                        show_message(error.getMessage());
                                    }
                                }
                        );
                        break;
                }
            }
        });

        return v;
    };


    private void send_to_server(String paytoken) {

        OkHttpClient client = new OkHttpClient();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(new Config().getFullApiUrl() + "do_order/").newBuilder();
        JSONObject params = new JSONObject();
        try {
            params.put("point_a", point_a);
            params.put("point_b", point_b);
            params.put("id_tariff", tariffUsed.getId());
            params.put("x1", x1);
            params.put("y1", y1);
            params.put("x2", x2);
            params.put("y2", y2);
            params.put("token", sharedPref.getString("token", ""));
            params.put("no_call",sharedPref.getBoolean("notCall", false));
            params.put("no_sms",sharedPref.getBoolean("notSms", false));
            params.put("price", String.format("%.2f", price));
            params.put("paytoken",paytoken);
        } catch (JSONException js) {
            js.printStackTrace();
        }

        RequestBody formBody = new FormBody.Builder()
                .add("params", params.toString())
                .build();
        Request request = new Request.Builder()
                .url(urlBuilder.build().toString())
                .post(formBody)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), e.getMessage() , Toast.LENGTH_SHORT).show();
                        preload_off();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    preload_off();
                    throw new IOException("Unexpected code " + response);
                } else {
                    String body = response.body().string();
                    try {
                        JSONObject answer = new JSONObject(body);
                        final String message = (answer.has("message"))? answer.getString("message") : null ;

                        if (answer.has("success")) {
                            preload_off();
                        }
                        if (answer.has("error")) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    preload_off();
                                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                }
                            });

                        }
                    } catch (JSONException js) {
                        js.printStackTrace();
                        preload_off();
                        show_message(js.getMessage());
                    }
                }
            }
        });


    }



    private void preload_on() {
        loading = ProgressDialog.show(getActivity(),"Отправляем","Пожалуйста, подождите.",false,false);
    }

    private void preload_off() {
        if (loading.isShowing()) {
            loading.dismiss();
        }
    }

    private void show_message(final String message){
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

            }
        });
    }


    /*
    * Заполнение значений из pref
    * */
    private void load_detail() {
        String nameTo = sharedPref.getString("orderPlaceNameTo", null);
        point_b = nameTo;
        String nameFrom = sharedPref.getString("orderPlaceNameFrom", null);
        point_a = nameFrom;
        Integer tips = sharedPref.getInt("tip", 0);
        Boolean isNal = sharedPref.getBoolean("isNal", false);

        Gson gson = new Gson();
        String json = sharedPref.getString("savedCard", null);
        Card card = null;
        if (json != null) {
            cardForPay = card = gson.fromJson(json, Card.class);
        }

        gson = new Gson();
        json = sharedPref.getString("orderTariff", null);
        Tariffs tariff = null;
        if (json != null) {
            tariff = gson.fromJson(json, Tariffs.class);
            tariffUsed = tariff;
        }

        gson = new Gson();
        json = sharedPref.getString("orderLatLngFrom", null);
        LatLng latLngFrom = null;
        if (json != null) {
            latLngFrom = gson.fromJson(json, LatLng.class);
            x1 = latLngFrom.latitude;
            y1 = latLngFrom.longitude;
        }

        gson = new Gson();
        json = sharedPref.getString("orderLatLngTo", null);
        LatLng latLngTo = null;
        if (json != null) {
            latLngTo = gson.fromJson(json, LatLng.class);
            x2 = latLngTo.latitude;
            y2 = latLngTo.longitude;
        }

        fromDetail.setText(nameFrom);
        toDetail.setText(nameTo);
        tipsL.setText(tips.toString() + "%");
        if (card != null && !isNal) {
            RadioButton rb  = new RadioButton(getActivity());
            rb.setText("Банковская карта \n **** **** **** " + card.getLast4());
            rb.setId(1);
            rb.setChecked(true);
            groupCash.addView(rb);
        }
        if ( tariff != null ) {
            tariffL.setText(tariff.getName());
        }


        float[] results = new float[1];
        Location.distanceBetween(latLngFrom.latitude, latLngFrom.longitude,
                latLngTo.latitude, latLngTo.longitude, results);

        float priceL = tariff.Raschet(results[0]);
        price = priceL;
        zakaz.setText("Сделать заказ (~" + String.format("%.2f", priceL) + " \u20BD)");
    }

    /*
    * запуск анимации
    */
    private void start_load_animation() {
        animateViewFadeIn(anim2, 100);
        animateViewFadeIn(anim3, 200);
        animateViewFadeIn(anim4, 300);
        animateViewFadeIn(anim5, 400);
        animateViewFadeIn(anim6, 500);
    }


    /*
    * Анимация
    * */
    private void animateViewFadeIn(View view, Integer delay) {
        AlphaAnimation animation = new AlphaAnimation(0.0f,1.0f);
        animation.setDuration(300);
        animation.setStartOffset(delay);
        view.startAnimation(animation);
    }

}
