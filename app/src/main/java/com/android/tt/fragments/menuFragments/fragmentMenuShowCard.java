package com.android.tt.fragments.menuFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.R;
import com.google.gson.Gson;
import com.stripe.android.model.Card;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenuShowCard extends Fragment {

    private TextView cardName;
    private TextView cardLast4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu_show_card, container, false);

        cardName = (TextView) v.findViewById(R.id.cardName);
        cardLast4 = (TextView) v.findViewById(R.id.last4);


        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String cardNameSh = sharedPref.getString("bankBrand", "");
        String cardLast4Sh = sharedPref.getString("bankLast4", "");



        cardName.setText(cardNameSh);
        cardLast4.setText("XXXX XXXX XXXX " + cardLast4Sh);

        v.findViewById(R.id.removeCardButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPref.edit().remove("bankBrand").commit();
                sharedPref.edit().remove("bankLast4").commit();
                sharedPref.edit().remove("savedCard").commit();
                Toast.makeText(getActivity(), "Карта откреплена", Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStack();
            }
        });

        return v;
    }

}
