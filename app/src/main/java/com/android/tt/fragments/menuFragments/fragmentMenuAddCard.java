package com.android.tt.fragments.menuFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.tt.R;


import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import io.fabric.sdk.android.Fabric;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenuAddCard extends Fragment {

    private MaterialRippleLayout but;
    private Button but_under;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Fabric.with(getActivity());
        final View v = inflater.inflate(R.layout.fragment_menu_card, container, false);

        final CardInputWidget mCardInputWidget = (CardInputWidget) v.findViewById(R.id.card_input_widget);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        but = (MaterialRippleLayout)  v.findViewById(R.id.addCardButton);
        but_under = (Button) v.findViewById(R.id.but_under);


        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Card cardToSave = mCardInputWidget.getCard();
                if (cardToSave == null) {
                    Toast.makeText(getActivity(), "Не удалось распознать карту", Toast.LENGTH_SHORT).show();
                } else {
                    if (cardToSave.validateCard()) {

                        //Stripe stripe = new Stripe(getActivity(), "pk_test_u1d3M4fFTcPUUX609AbZVkTN");

                        but.setClickable(false);
                        but.setFocusableInTouchMode(false);
                        but_under.setText("Сохраняем...");

                        SharedPreferences.Editor editor = sharedPref.edit();
                        String last4 = cardToSave.getLast4();
                        String brand = cardToSave.getBrand();

                        editor.putString("bankLast4", last4 );
                        editor.putString("bankBrand", brand );
                        Gson gson = new Gson();
                        String json = gson.toJson(cardToSave);
                        editor.putString("savedCard", json);
                        editor.commit();
                        Toast.makeText(getActivity(), "Карта сохранена" , Toast.LENGTH_LONG).show();
                        getFragmentManager().popBackStack();

//                        stripe.createToken(
//                                cardToSave,
//                                new TokenCallback() {
//                                    public void onSuccess(Token token) {
//                                        String last4 = cardToSave.getLast4();
//                                        String brand = cardToSave.getBrand();
//
//                                    }
//                                    public void onError(Exception error) {
//                                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
//                                    }
//                                }
//                        );

                    }
                }
            }
        });


        return v;
    }

}
