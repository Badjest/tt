package com.android.tt.fragments.menuFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tt.AuthActivity;
import com.android.tt.R;
import com.digits.sdk.android.Digits;


/**
 * Created by JoshRonaldMine on 03.02.2017.
 */

public class fragmentMenuPhone extends Fragment {

    private TextView phone;
    private TextView name;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_menu_phone, container, false);
        phone = (TextView) v.findViewById(R.id.phone);
        name = (TextView) v.findViewById(R.id.name);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String phoneSh = sharedPref.getString("phoneNumber", "");
        String nameSh = sharedPref.getString("userName", "");


        phone.setText(phoneSh);
        name.setText(nameSh);

        /*Кнопка выхода*/
        v.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setChoise(sharedPref);
            }
        });

        return v;
    }

    private void setChoise(final SharedPreferences sh) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Все персональные настройки будут потеряны. Предстоит вновь пройти процедуру верификации!!!")
                .setTitle("Вы уверены?");
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton("Принять", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sh.edit().clear().commit();
                Digits.logout();
                Intent i = new Intent(getActivity(), AuthActivity.class);
                getActivity().finish();
                startActivity(i);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
