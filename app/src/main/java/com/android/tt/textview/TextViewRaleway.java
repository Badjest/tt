package com.android.tt.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.android.tt.R;


/**
 * Created by JM on 26.03.2016.
 */
//Текстовые поля с определенным шрифтом
public class TextViewRaleway extends TextView {

    public TextViewRaleway(Context context) {
        this(context, null, 0);
    }

    public TextViewRaleway(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public TextViewRaleway(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont(context);
    }

    private void setFont(Context context) {
        Typeface face = Typefaces.get(context, context.getText(R.string.font_bold).toString());
        setTypeface(face);
    }
}
