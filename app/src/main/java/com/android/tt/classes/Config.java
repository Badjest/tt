package com.android.tt.classes;

/**
 * Created by JoshRonaldMine on 22.03.2017.
 */

public class Config {

    private String server_url = "http://u999470h.bget.ru/";
    private String server_api = "app-api/";
    private String api_version = "v1/";

    private String getServer_url() {
        return server_url;
    }

    private String getServer_api() {
        return server_api;
    }

    private String getApi_version() {
        return api_version;
    }

    public String getFullApiUrl() {
        return getServer_url() + getServer_api() + getApi_version();
    }

}
