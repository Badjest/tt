package com.android.tt.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JoshRonaldMine on 17.05.2017.
 */

public class returnJsonArray {

    public JSONArray getJsonArray(String string) {
        JSONArray JsonArray = null;
        JSONObject JsonObject = null;

        try {

            JsonArray = new JSONArray(string);

        } catch (JSONException je1) {
            je1.printStackTrace();
            try {

                JsonObject = new JSONObject(string);
                JsonArray = new JSONArray();
                JsonArray.put(JsonObject);

            } catch (JSONException je2) {

                je2.printStackTrace();
                try {
                    JsonObject = new JSONObject();
                    JsonArray = new JSONArray();
                    JsonObject.put("code", "no_parse");
                    JsonObject.put("message", "Не удалось конвертировать JSON объект");
                    JsonObject.put("error", false);
                    JsonArray.put(JsonObject);
                } catch (JSONException je3) {
                    je3.printStackTrace();
                }

            }
        }
        return JsonArray;
    }
}
